package pagereplacealgos;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.*;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Aryan
 */
public class FIFO {

	FIFO(){
		//for console!!!
//		fifoPageInitialize();
	}

    /**
     *
     * @param args
     */
    public static void main(String args[])
    {
    	new FIFO();
    }

    //The below method is for just doing this on the console!!!
    void fifoPageInitialize(){
    	Scanner scan = new Scanner(System.in);
    	System.out.println("Enter the number of page references:");
    	int num = scan.nextInt();
    	scan.nextLine();
    	System.out.println("Enter the" + num +" pages:");
    	ArrayList<Integer> pages = new ArrayList<>();
    	for(int i = 0 ;i<num ;i++){
    		pages.add(scan.nextInt());
    		scan.nextLine();
    	}
    	System.out.println("Enter the page frame size:");
    	int pageFrameSize = scan.nextInt();
    	scan.nextLine();
//    	fifoPageReplacementWorking(pages,pageFrameSize);
    }
    /**
     * This functions all the working of the FIFO page replacement algo!!!
     * @param pages : This contains the ArrayList of all the pages!!!
     * @param pageFrameSize : This contains the pageFrameSize enter by the user in the spinner!!!
     * @param main_frame_container : This is JPanel in which directly represents the memory!!!
     * @param panel_container : This is JPanel in which the number of the page hit and the page hit ratio is shown!!!
     */
    void fifoPageReplacementWorking(ArrayList<Integer> pages,int pageFrameSize,JPanel main_frame_container,JPanel panel_container){
    	int current = -1,flag = 0;
    	boolean pageHit = false;
    	int numOfPageHits = 0;
    	int memory[] = new int[pageFrameSize];
    	for (int page : pages) {
    		//check if the page is already there in the memory!!!
    		int i;
    		if(flag == 0){
	    		for(i =0;i<=current;i++){
	    			if(memory[i] == page){
    					pageHit = true;
    					numOfPageHits++;//For hit ratio!!!
                                        displayPages(memory,current+1,main_frame_container,pages);
    					break;
    				}
    			}
    		}else{
	    		for(i =0;i<memory.length;i++){
	    			if(memory[i] == page){
    					pageHit = true;
    					numOfPageHits++;
                                        displayPages(memory,memory.length,main_frame_container,pages);
    					break;
    				}
    			}
    		}
    		if(pageHit == false){
                        current++;
                        if(current == pageFrameSize){
    				current = 0;
    				flag = 1;
    			}
    			memory[current] = page;
    			if(flag == 0)
	    			displayPages(memory,current+1,main_frame_container,pages);
	    		else
	    			displayPages(memory,memory.length,main_frame_container,pages);
    		}else{
    			pageHit = false;
    		}
    	}
        double hitRatio = calculatePageHitRatio(numOfPageHits,pages.size());
        panel_container.setLayout(new GridLayout(2,1));
        panel_container.removeAll();
        JLabel l = new JLabel("The Number of Page Hits is:"+numOfPageHits,JLabel.LEFT);
        l.setFont(new Font("Lato",Font.PLAIN,14));
        panel_container.add(l);
        l = new JLabel("The Hit Ratio is:"+hitRatio+"%",JLabel.LEFT);
        panel_container.add(l);
        l.setFont(new Font("Lato",Font.PLAIN,14));
        panel_container.validate();
        panel_container.repaint();

    }
    /**
     * This functions does the work of putting the data inside the UI after the processing is done!!!
     * @param memory :This is the integer array containing the answer to be put in UI!!!
     * @param size : This is a int value indicating the page frame size!!!
     * @param main_frame_container : This is the JPanel directly representing the memory!!!
     * @param pages : This is the ArrayList representing all the pages entered by user!!!
     */
    void displayPages(int memory[],int size,JPanel main_frame_container,ArrayList<Integer> pages){
        main_frame_container.setLayout(new GridLayout(pages.size()/5, 5));
	    pageBox1 obj = new pageBox1(memory,size,pages);                
	    main_frame_container.add(obj);                
	    main_frame_container.validate();
	    main_frame_container.updateUI();
    }
    double calculatePageHitRatio(int numOfPageHits,int numPages){
  
    	double tp = (double)numOfPageHits/(numPages);
    	//If we donot typecast to double then the 0 will come!!!

    	double hitRatio = tp*100;
    	System.out.println("Page Hit Ratio is:"+hitRatio+"%");//console
        return hitRatio;
    }
}
