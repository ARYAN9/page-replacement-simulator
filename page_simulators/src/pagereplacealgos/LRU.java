package pagereplacealgos;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.*;
import javax.swing.JLabel;
import javax.swing.JPanel;
import pagereplacealgos.pageBox1;

/**
 *
 * @author Aryan
 */
public class LRU {
	LRU(){
		//for console!!!
//		lruPageInitialize();
	}

    /**
     *
     * @param args
     */
    public static void main(String args[])
    {
    	new LRU();
    }
    //The below method is for just doing this on the console!!!
    void lruPageInitialize(){
    	Scanner scan = new Scanner(System.in);
    	System.out.println("Enter the number of page references:");
    	int num = scan.nextInt();
    	scan.nextLine();
    	System.out.println("Enter the" + num +" pages:");
    	ArrayList<Integer> pages = new ArrayList<>();
    	for(int i = 0 ;i<num ;i++){
    		pages.add(scan.nextInt());
    		scan.nextLine();
    	}
    	System.out.println("Enter the page frame size:");
    	int pageFrameSize = scan.nextInt();
    	scan.nextLine();
//    	lruPageReplacementWorking(pages,pageFrameSize);
    }
    /**
     * This functions all the working of the LRU page replacement algo!!!
     * @param pages : This contains the ArrayList of all the pages!!!
     * @param pageFrameSize : This contains the pageFrameSize enter by the user in the spinner!!!
     * @param main_frame_container : This is JPanel in which directly represents the memory!!!
     * @param panel_container : This is JPanel in which the number of the page hit and the page hit ratio is shown!!!
     */

    void lruPageReplacementWorking(ArrayList<Integer> pages,int pageFrameSize,JPanel main_frame_container,JPanel panel_container){
    	int current = -1,flag = 0;
    	boolean pageHit = false;
    	int numOfPageHits = 0;
    	int memory[] = new int[pageFrameSize];
    	int count[] = new int[pageFrameSize];//how many time page come!!!

    	for (int page : pages) {
    		//check if the page is already there in the memory!!!
    		int i;
    		if(flag == 0){
	    		for(i =0;i<=current;i++){
	    			if(memory[i] == page){
    					pageHit = true;
    					numOfPageHits++;//For hit ratio!!!
    					count[i] = 1;
                        int index = i;
    					for(i =0;i<=current;i++){
    						if(i != index){
    							count[i] += 1;
    						}
    					}
                        displayPages(memory,current+1,main_frame_container,pages);
    					break;
    				}
    			}
    		}else{
	    		for(i =0;i<memory.length;i++){
	    			if(memory[i] == page){
    					pageHit = true;
    					numOfPageHits++;
    					count[i] = 1;
                                        int index = i;
    					for(i =0;i<memory.length;i++){
    						if(i != index){
    							count[i] += 1;
    						}
    					}
                        displayPages(memory,memory.length,main_frame_container,pages);
    					break;
    				}
    			}
    		}
    		if(pageHit == false){
	    		current++;
	    		if(current == pageFrameSize){
    				current = 0;
    				flag = 1;
    			}
    			int big,index=0;
    			if(flag == 1){
	    			big = count[0];
	    			for(i = 1 ;i<pageFrameSize;i++){
	    				if(big < count[i]){
	    					big = count[i];
	    					index = i;
	    				}
	    			}
    			}
    			if(flag == 1){
    				memory[index] = page;
    				count[index] = 0;
    			}
    			else {
    				memory[current] = page;
    				count[current] = 0;
    			}
    			
    			if(flag == 1){
    				for(i = 0;i<count.length;i++){
    					count[i] += 1;
    				}
    			}else{
	    			for(i = 0 ;i<=current;i++){
	    				count[i] += 1;
	    			}    				
    			}

    			if(flag == 0)
	    			displayPages(memory,current+1,main_frame_container,pages);
	    		else
	    			displayPages(memory,memory.length,main_frame_container,pages);
    		}else{
    			pageHit = false;
    		}
    	}
    	double hitRatio = calculatePageHitRatio(numOfPageHits,pages.size());
        panel_container.setLayout(new GridLayout(2,1));
        panel_container.removeAll();
        JLabel l = new JLabel("The Number of Page Hits is:"+numOfPageHits,JLabel.LEFT);
        l.setFont(new Font("Lato",Font.PLAIN,14));
        panel_container.add(l);
        l = new JLabel("The Hit Ratio is:"+hitRatio+"%",JLabel.LEFT);
        panel_container.add(l);
        l.setFont(new Font("Lato",Font.PLAIN,14));
        panel_container.validate();
        panel_container.repaint();
    }
    /**
     * This functions does the work of putting the data inside the UI after the processing is done!!!
     * @param memory :This is the integer array containing the answer to be put in UI!!!
     * @param size : This is a int value indicating the page frame size!!!
     * @param main_frame_container : This is the JPanel directly representing the memory!!!
     * @param pages : This is the ArrayList representing all the pages entered by user!!!
     */
    void displayPages(int memory[],int size,JPanel main_frame_container,ArrayList<Integer> pages){
        main_frame_container.setLayout(new GridLayout(pages.size()/5, 5));
	    pageBox1 obj = new pageBox1(memory,size,pages);
	    main_frame_container.add(obj);                
        main_frame_container.validate();
        main_frame_container.updateUI();
    }
    double calculatePageHitRatio(int numOfPageHits,int numPages){
  
    	double tp = (double)numOfPageHits/(numPages);
    	//If we donot typecast to double then the 0 will come!!!
    	double hitRatio = tp*100;
        return hitRatio;
    }
}
